## **cutie-dead-synth**

**Play it on your computer or laptop keyboard**

A PyQt5 synthesizeresque. A version of another synth I previously
coded with tkinter as the GUI, (quarter-dead-synth).

**_This may not work properly on some distros._** Seems to work OK on Debian
and Ubuntu Studio, so I think it needs a fast or low latency kernel.

Toggle between the key of E4 and C4. Swing between sine and triangle waves.
Switch sine to an octave lower or some octaves higher.

Set Output Device, Blocksize and samplerate from the dialog accessed from the drop down menu.
Maybe for a USB audio interface for example. Stuff > Set Output Device, Blocksize
and Samplerate Click on the required device from the list then click **Set / Close**
The blocksize can be adjusted with the slider. The blocksize is how many samples
is passed to the sound buffer at a time. If the blocksize is too small you
will hear skips or crackles because the buffer has not had enough time to be
filled up before the next callback. If the blocksize is too big, there will be a lag
between hitting the key and hearing the sound.
**_Note_**: If you close the dialogue using the **X** in the top right corner, the
stream will fail to restart. Reopen the dialogue, repeat your selection or
whatever and click **Set / Close** to restart the stream.


### Configure the keys to match any keyboard

The keyboard configuration is for a qwerty keyboard but you can change that in the Stuff
menu. To change the key bindings click on **custom keybinder** in the **stuff**
drop down menu then **Select Key** if needed, then just type in the keys in order
of the ascending scale. The dialog will automatically close when all the
18 notes has been assigned keys. Next click the **Change Key** button to update
the configuration. From the drop down menu the new custom configuration can be
saved as a preset so it can used again next time.

![kb-e-photo](images/kb_e.jpg)

### Save and Recall Settings

**Save current settings** Stuff > Save As Presets. A dialogue will open. A snapshot
of the sliders and key (C or E) is made the moment the dialogue was opened.
Give the presets a name and click **Save**. Close dialogue. The file will be
saved to the presets directory found in the cutie-dead-synth project directory.

**Recall presets** Stuff > Recall Presets. Click on the required preset.pickle
file from the list. the dialogue will close. Click **Apply Settings** in the main
window.

**Get PyQt5**

```
sudo apt-get install python3-pyqt5
```

**>= python3.6**

```
pip3 install numpy
pip3 install sounddevice
pip3 install mido
pip3 install scipy
```

If computer says "NO" then set up a venv and try again.

```
sudo apt install python3-pip
sudo apt install python3-venv
python3 -m venv venv
source venv/bin/activate

pip3 install numpy
pip3 install sounddevice
pip3 install mido
pip3 install scipy
pip3 install PyQt5
```

### Midi Input

**python3.6 python3.7**

```
pip3 install python-rtmidi
```

**Open Midi Input Port** from the drop down menu. select the
input device name and the window will close. Now click **Apply Settings** in the
main window, wait a few seconds, and its ready.

**Run cutie-dead-synth**

```
python3 cutie_dead_synth.py
```

![cutie-dead-photo](images/cutie-dead-synth.jpg)

### Keyboard Shortcuts

| Key     |Function     |
|:--------|:------------|
| Ctrl-q  | Quit        |
| Ctrl-k  | Change key  |
| Ctrl-a  | Apply       |

=========================================================================

**extra_thing_for_fun.py**
This is my favourite one. Its not exactly conventional but I think it sounds
far far better. It uses simpleaudio. It uses your computer keyboard or plug in
a midi keyboard and click the **Open Midi Input** button and select the
input device name, wait a few seconds and its ready.

**Supported samplerates**

+ 44100
+ 48000
+ 88200
+ 96000
+ 192000

**Linux prerequisites for simpleaudio**

```
pip3 install --upgrade pip setuptools
sudo apt-get install -y python3-dev libasound2-dev
```
**simpleaudio and mido**

```
pip3 install simpleaudio
pip3 install mido
pip3 install python-rtmidi
```

### Keyboard Shortcuts

| Key     |Function                        |
|:--------|:-------------------------------|
| Ctrl-q  | Quit                           |
| Ctrl-k  | Change key                     |
| Ctrl-a  | Apply sliders                  |
| Ctrl-m  | Monophonic / polyphonic toggle |


**run**

```
python3 extra_thing_for_fun.py
```

Or with a samplerate of 44100 for example, instead of 48000 (default)

```
python3 extra_thing_for_fun.py 44100
```

### Save Notes as .wav Files
Click this button and it will create a directory named with a time stamp and
sample rate then fill it full of wav files each containing the individual notes.

Suppose you can deactivate the venv when finished.

```
deactivate
```
